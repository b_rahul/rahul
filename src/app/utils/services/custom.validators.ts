import { AbstractControl, ValidationErrors, FormGroup, FormControl } from "@angular/forms";

export function UrlValidator(
  control: AbstractControl
): ValidationErrors | null {
  if (control.value) {
    if (!control.value.startsWith("https") || !control.value.includes(".me")) {
      return { invalid: true };
    } else {
      return null;
    }
  } else {
    return null;
  }
}
export function MobileValidator(
  control: AbstractControl
): ValidationErrors | null {
  if (control.value) {
    if (!control.value.toString().match(/^[0-9]+(\.?[0-9]+)?$/)) {
      return { invalid: true };
    } else {
      return null;
    }
  } else {
    return null;
  }
}
export function EmailValidator(
  control: AbstractControl
): ValidationErrors | null {
  if (control.value) {
    if (
      !control.value
        .toString()
        .match(/^\w+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,8}$/)
    ) {
      return { invalid: true };
    } else {
      return null;
    }
  } else {
    return null;
  }
}
export function AlphaValidator(
  control: AbstractControl
): ValidationErrors | null {
  if (control.value) {
    if (!control.value.toString().match(/^[-a-zA-Z]+(( ?)+[-a-zA-Z]+)*$/)) {
      return { AlphaValidator: true };
    } else {
      return null;
    }
  } else {
    return null;
  }
}
export function NumericValidator(
  control: AbstractControl
): ValidationErrors | null {
  if (control.value) {
    if (!control.value.toString().match(/^[0-9]+$/)) {
      return { NumericValidator: true };
    } else {
      return null;
    }
  } else {
    return null;
  }
}

export function DecimalValidator(
  control: AbstractControl
): ValidationErrors | null {
  if (control.value != null) {
    if (!control.value.toString().match(/^\d*\.?\d*$/)) {
      return { DecimalValidator: true };
    } else {
      return null;
    }
  } else {
    return null;
  }
}

export function AlphaNumericValidator(
  control: AbstractControl
): ValidationErrors | null {
  if (control.value) {
    if (
      !control.value.toString().match(/^[-a-zA-Z0-9]+(( ?)+[-a-zA-Z0-9]+)*$/)
    ) {
      return { AlphaNumericValidator: true };
    } else {
      return null;
    }
  } else {
    return null;
  }
}
export function AllowedCharacterValidator(
  control: AbstractControl
): ValidationErrors | null {
  if (control.value) {
    if (
      !control.value
        .toString()
        .match(
          /^[-a-zA-Z0-9$-/:-{-~!"^_`\[\]]+(( ?)+[-a-zA-Z0-9$-/:-{-~!"^_`\[\]]+)*$/
        )
    ) {
      return { AllowedCharacterValidator: true };
    } else {
      return null;
    }
  } else {
    return null;
  }
}
export function PasswordValidator(
  control: AbstractControl
): ValidationErrors | null {
  if (control.value) {
    let password = control.value.toString();
    const regex = /[$-/:-?{-~!"^_@`\[\]]/g;
    const lowerLetters = /[a-z]+/.test(password);
    const upperLetters = /[A-Z]+/.test(password);
    const numbers = /[0-9]+/.test(password);
    const symbols = regex.test(password);

    if (password.length < 6) {
      return { PasswordValidator: true, minLength: true };
    }
    else if (password.length > 32) {
      return { PasswordValidator: true, maxLength: true };
    }
    else if (!lowerLetters) {
      return { PasswordValidator: true, lowerCase: true };
    }
    else if (!upperLetters) {
      return { PasswordValidator: true, upperCase: true };
    }
    else if (!numbers) {
      return { PasswordValidator: true, numbers: true };
    }
    else if (!symbols) {
      return { PasswordValidator: true, symbols: true };
    }
    else {
      return null;
    }
  } else {
    return null;
  }
}
export function PasswordValidation(password: string) {
  const regex = /[$-/:-?{-~!"^_@`\[\]]/g;
  const lowerLetters = /[a-z]+/.test(password);
  const upperLetters = /[A-Z]+/.test(password);
  const numbers = /[0-9]+/.test(password);
  const symbols = regex.test(password);

  if (
    password.length >= 6 &&
    password.length < 32 &&
    lowerLetters &&
    upperLetters &&
    numbers &&
    symbols
  ) {
    return true;
  } else {
    return false;
  }
}

export function FormErrorMessage(formGroup: FormGroup, control: string) {
  let controlValue = formGroup.get(control).errors;
  if (controlValue) {
    if (controlValue.required) {
      return 'This field is required'
    }
    else if (controlValue.minlength) {
      return 'Please enter at least ' + controlValue.minlength.requiredLength + ' characters';
    }
    else if (controlValue.maxlength) {
      return 'Please enter not more than  ' + controlValue.maxlength.requiredLength + ' characters';
    }
    else if (controlValue.email) {
      return 'Please enter valid email'
    }
    else if (controlValue.UrlValidator) {
      return 'Please enter valid url'
    }
    else if (controlValue.MobileValidator) {
      return 'Please enter valid mobile number'
    }
    else if (controlValue.AlphaValidator) {
      return 'Please enter only alphabets'
    }
    else if (controlValue.NumericValidator) {
      return 'Please enter only number'
    }
    else if (controlValue.DecimalValidator) {
      return 'Please enter only number or decimal'
    }
    else if (controlValue.AlphaNumericValidator) {
      return 'Please enter only alphabets and numbers'
    }
    else if (controlValue.AllowedCharacterValidator) {
      return "Please use only '$-/:-{-~!\"^_`\[\]' special characters";
    }
    else if (controlValue.PasswordValidator) {
      return 'Password must be between 6 - 32 character, Contains 1 Uppercase 1 Lowercase 1 Number & 1 Special character';
    }
    else {
      return controlValue;
    }
  }
}
