import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  constructor() { }

  AuthURL: string = "/Auth/";
  Organization: string = "/Organization/";
  Common: string = "/Common/";

}
