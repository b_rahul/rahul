import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, finalize, timeout, map, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NotificationService } from '../services/notification.service';
import { EncryptionService } from '../services/encryption.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements HttpInterceptor {

  private permittedActions: string[] = ["POST", "GET", "PUT", "DELETE"];

  constructor(
    private _router: Router,
    private _http: HttpClient,
    private _notification: NotificationService,
    private _encrypt: EncryptionService
  ) { }

  getBaseUrl() {
    let baseUrlArr = this._router.url.split('/');
    if (baseUrlArr.length >= 3) {
      return '/' + baseUrlArr[1] + '/' + baseUrlArr[2];
    }
    else if (baseUrlArr.length = 2) {
      return '/' + baseUrlArr[1];
    }
    else {
      return '/'
    }
  }

  getHeader(type: string[]) {
    let headers = new HttpHeaders();
    if (type.includes('application/json')) {
      headers = headers.set('Content-Type', 'application/json');
    }
    if (type.includes('application/x-www-form-urlencoded') && !headers.get('Content-Type')) {
      headers = headers.set('Content-Type', 'application/x-www-form-urlencoded');
    }
    if (type.includes('text/plain') && !headers.get('Content-Type')) {
      headers = headers.set('Content-Type', 'text/plain');
    }
    if (type.includes('text/html') && !headers.get('Content-Type')) {
      headers = headers.set('Content-Type', 'text/html');
    }
    if (type.includes('multipart/form-data') && !headers.get('Content-Type')) {
      headers = headers.set('Content-Type', 'multipart/form-data');
    }

    return headers;
  }

  getData() {
    return this.getValueFromSessionStorage('data');
  }

  getAuthToken() {
    if (this.getData()) {
      return JSON.parse(this.getData()).accessToken;
    }
  };

  getMemberId() {
    if (this.getData()) {
      return JSON.parse(this.getData()).memberId;
    }
  };

  logout() {
    if (this.getAuthToken()) {
      this.delete(`${environment.baseUrl}/v1/Member`).subscribe();
    }
    window.sessionStorage.clear();
    this._router.navigate(['/Login']);
  };

  setValueToSessionStorage(key: string, value: string) {
    window.sessionStorage.setItem(key, this._encrypt.encryptData(value));
  };

  getValueFromSessionStorage(key: string) {
    if (window.sessionStorage.getItem(key)) {
      return this._encrypt.decryptData(window.sessionStorage.getItem(key));
    }
  };

  clearStorage() {
    window.sessionStorage.clear();
  }

  private prepareHeader(method: string, url: string, headers: HttpHeaders | null): HttpHeaders {
    headers = headers || new HttpHeaders();
    let authToken: string = this.getAuthToken();

    headers = headers.set('language', 'en')
    if (authToken) {
      headers = headers.set("Authorization", authToken);
      headers = headers.set("id", this.getMemberId());
    }

    return headers
  }

  get<T>(url: string, headers?: HttpHeaders | null, options?: HttpParams): Observable<T> {
    const expandedHeaders = this.prepareHeader('GET', url, headers);
    return this._http.get<T>(url, { headers: expandedHeaders, params: options, withCredentials: false });
  }

  post<T>(url: string, body: any, headers?: HttpHeaders | null): Observable<T> {
    const expandedHeaders = this.prepareHeader('POST', url, headers);
    return this._http.post<T>(url, body, { headers: expandedHeaders, withCredentials: false });
  }

  put<T>(url: string, body: any, headers?: HttpHeaders | null): Observable<T> {
    const expandedHeaders = this.prepareHeader('PUT', url, headers);
    return this._http.put<T>(url, body, { headers: expandedHeaders, withCredentials: false });
  }

  delete<T>(url: string, headers?: HttpHeaders | null): Observable<T> {
    const expandedHeaders = this.prepareHeader('DELETE', url, headers);
    return this._http.delete<T>(url, { headers: expandedHeaders, withCredentials: false });
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (!this.permittedActions.includes(request.method)) {
      return;
    }

    $('.loading').show();

    return next.handle(request)
      .pipe(
        tap((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse && event.status === 200) {
            if (event.body.message) {
              this._notification.success(event.body.message);
            }
          }
        }),
        catchError((response) => {
          if (response.status === 400 || response.status === 422) {
            if (response.error != null) {
              var html: string = "";

              if (response.status === 422) {
                Object.keys(response.error.errors).forEach((key: string, index: number) => {
                  html += key + ": " + response.error.errors[key]["msg"] + "</br>";
                });
              }
              else {
                html += response.error["message"] + "</br>";
              }

              this._notification.warning(html)
            }
          }
          else if (response.status === 401) {
            this._notification.error("Unauthorized");
            this.logout();
          }
          else if (response.status === 404) {
            this._notification.info("Service is Temporarily unavailable.");
          }
          return throwError(response);
        }),
        timeout(15000),
        finalize(() => $('.loading').hide())
      )
  };

}
