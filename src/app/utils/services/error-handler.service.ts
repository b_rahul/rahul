import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';
import * as StackTrace from 'stacktrace-js';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerService implements ErrorHandler {
  constructor(private _injector: Injector) { }

  handleError(error: any) {
    const location = this._injector.get(LocationStrategy);
    const message = error.message ? error.message : error.toString();
    const url = location instanceof PathLocationStrategy ? location.path() : '';
    console.log(error.url, error.headers)
    if (!error.url && !error.headers) { 
      StackTrace.fromError(error).then(stackframes => {
        const stackString = stackframes
          .splice(0, 20)
          .map((sf) => {
            return sf.toString();
          }).join('\n');
        console.log({ message, url, stack: stackString });
      });
    }
    else {
      console.log({ message, url});
    }
    
    throw error;
  }
}
