import { Injectable } from '@angular/core';
import { ConfigService } from 'src/app/utils/services/config.service';

@Injectable({
  providedIn: 'root'
})
export class GuardFactory {

  constructor(
    private _config: ConfigService
  ) { }

}