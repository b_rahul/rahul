import { Injectable } from "@angular/core";
import {
  CanActivate,
  CanActivateChild,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from "@angular/router";
import { Observable } from "rxjs";
import { AuthService } from "../services/auth.service";
import { GuardFactory } from "./guard.service";
import { EncryptionService } from "../services/encryption.service";

@Injectable({
  providedIn: "root",
})
export class AuthGuard implements CanActivate, CanActivateChild {
  constructor(
    private _router: Router,
    private _authService: AuthService,
    private _factory: GuardFactory,
    private _encryptService: EncryptionService
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    let data = this._authService.getValueFromSessionStorage("data");
    let userData: any;
    if (data != null) {
      userData = JSON.parse(data);
    }

    if (userData) {
      let basePath = state.url.split("/").slice(1);
      if (basePath[0].toString() == "App") {
        if (
          basePath[1].toString() == "Admin" &&
          userData.roles.includes("admin")
        ) {
          return true;
        } else if (
          basePath[1] == "User" &&
          userData.roles.includes("user")
        ) {
          return true;
        } else {
          this._router.navigate(["/"]);
          return false;
        }
      } else {
        this._router.navigate(["/"]);
        return false;
      }
    } else {
      this._router.navigate(["/"]);
      return false;
    }
  }

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    const authToken = this._authService.getAuthToken();
    let data = this._authService.getValueFromSessionStorage("data");
    let userData: any;
    if (data != null) {
      userData = JSON.parse(data);
    }

    if (
      authToken &&
      userData &&
      userData.userAccessRole != undefined &&
      userData.userAccessRole != null
    ) {
      let basePath = state.url.split("/").slice(1);
      if (basePath[0].toString() == "App") {
        if (
          basePath[1].toString() == "Admin" &&
          userData.userAccessRole == "isAdmin"
        ) {
          return true;
        } else if (
          basePath[1] == "User" &&
          userData.userAccessRole == "isUser"
        ) {
          return true;
        } else {
          this._router.navigate(["/"]);
          return false;
        }
      } else {
        this._router.navigate(["/"]);
        return false;
      }
    } else {
      this._router.navigate(["/"]);
      return false;
    }
  }
}
