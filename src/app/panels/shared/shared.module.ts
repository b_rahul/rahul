import { NgModule, ErrorHandler } from "@angular/core";
import { AccordionAnchorDirective, AccordionLinkDirective, AccordionDirective } from './accordion';

import { AppMaterialModule } from "src/app/app-material-modules";
import { CommonModule } from '@angular/common';
import { SpinnerComponent } from './spinner.component';
import { RouterModule } from '@angular/router';
import { NotFoundComponent } from './not-found/not-found.component';

@NgModule({
  declarations: [
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
    SpinnerComponent,
    NotFoundComponent
  ],
  imports:[
    AppMaterialModule,
    CommonModule,
    RouterModule.forChild([])
  ],
  exports: [
    AccordionAnchorDirective,
    AccordionLinkDirective,
    AccordionDirective,
    SpinnerComponent
  ],
  providers: []
})
export class SharedModule {}