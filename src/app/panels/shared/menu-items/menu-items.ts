import { Injectable } from '@angular/core';

export interface Menu {
  state: string;
  name: string;
  type: string;
  icon: string;
}

const MENUITEMS = [
  { state: 'Dashboard', type: 'link', name: 'Dashboard', icon: 'dashboard' },
  { state: 'Banner', type: 'link', name: 'Banner', icon: 'view_carousel' },
  { state: 'Category', type: 'link', name: 'Categories', icon: 'category' },
  { state: 'Product', type: 'link', name: 'Products', icon: 'view_list' },
  { state: 'User', type: 'link', name: 'User', icon: 'group' }
];

@Injectable()
export class MenuItems {
  getMenuitem(): Menu[] {
    return MENUITEMS;
  }
}
