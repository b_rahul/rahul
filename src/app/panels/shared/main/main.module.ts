import { NgModule, ErrorHandler } from "@angular/core";
import { MainComponent } from "./main.component";
import { AppHeaderComponent } from "./header/header.component";
import { AppSidebarComponent } from "./sidebar/sidebar.component";
import { AppMaterialModule } from "src/app/app-material-modules";
import { SharedModule } from "../shared.module";
import { RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";
import { MenuItems } from '../menu-items/menu-items';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DataTablesModule } from 'angular-datatables';

@NgModule({
  declarations: [MainComponent, AppHeaderComponent, AppSidebarComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    AppMaterialModule,
    FlexLayoutModule,
    DataTablesModule,
    RouterModule.forChild([])
  ],
  providers: [MenuItems],
})
export class MainModule {}
