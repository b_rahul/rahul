import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/utils/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: []
})
export class AppHeaderComponent implements OnInit {

  public baseUrl: string;

  constructor(
    private _router: Router,
    private _authService: AuthService,
  ) { }

  ngOnInit() {
    this.baseUrl = this._authService.getBaseUrl();
  }

  logout() {
    this._authService.logout();
  }

}
