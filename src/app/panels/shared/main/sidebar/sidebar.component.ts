import {
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit
} from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { MenuItems } from '../../menu-items/menu-items';
import { AuthService } from 'src/app/utils/services/auth.service';
import { AdminService } from 'src/app/panels/admin/admin.service';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: []
})
export class AppSidebarComponent implements OnInit, OnDestroy {
  mobileQuery: MediaQueryList;

  private _mobileQueryListener: () => void;
  public baseUrl: string;
  public userFullName: string;

  constructor(
    private _authService: AuthService,
    private _AdminService: AdminService,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    public menuItems: MenuItems
  ) {
    this.mobileQuery = media.matchMedia('(min-width: 768px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit() {
    this.baseUrl = this._authService.getBaseUrl();
    this.getProfile();
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  getProfile() {
    this._AdminService.getProfile()
      .subscribe((apiResponse) => {
        this.userFullName = apiResponse.firstName.toUpperCase() + " " + apiResponse.lastName.toUpperCase();
      });
  }

  logout() {
    this._authService.logout();
  }
}
