import { NgModule, ErrorHandler } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";

import { AppMaterialModule } from "src/app/app-material-modules";
import { LoginComponent } from "./login/Login.component";
import { SharedModule } from "../shared/shared.module";
import { VerifyEmailComponent } from "./verify-email/verify-email.component";
import { SetPasswordComponent } from "./set-password/set-password.component";
import { IndexComponent } from "./index/index.component";
import { SendEmailComponent } from "./send-email/send-email.component";
import { NonAuthGuard } from 'src/app/utils/guards/non-auth.guard';

const FrontRoutes: Routes = [
  {
    path: "Login",
    canActivate: [NonAuthGuard],
    component: LoginComponent
  },
  {
    path: "Verify-Email/:Token/:MemberID",
    canActivate: [NonAuthGuard],
    component: VerifyEmailComponent
  },
  {
    path: "Set-Password/:Token/:MemberID",
    canActivate: [NonAuthGuard],
    component: SetPasswordComponent
  },
  {
    path: "Resend-Email",
    canActivate: [NonAuthGuard],
    component: SendEmailComponent
  },
  {
    path: "Forgot-Password",
    canActivate: [NonAuthGuard],
    component: SendEmailComponent
  },
  {
    path: "",
    canActivate: [NonAuthGuard],
    component: IndexComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    AppMaterialModule,
    FlexLayoutModule,
    RouterModule.forChild(FrontRoutes)
  ],
  declarations: [
    LoginComponent,
    VerifyEmailComponent,
    SetPasswordComponent,
    IndexComponent,
    SendEmailComponent
  ],
  providers: [],
})
export class FrontModule {}
