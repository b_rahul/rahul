import { Injectable } from "@angular/core";
import { AuthService } from "src/app/utils/services/auth.service";
import { environment } from "src/environments/environment";
import { Observable } from "rxjs";
import { HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: "root",
})
export class FrontService {
  constructor(private _authService: AuthService) { }

  verifyEmail(data: any): Observable<any> {
    return this._authService.post(
      `${environment.baseUrl}/v1/Member/VerifyEmail`,
      data
    ).pipe(map(res => res));
  }
  resendEmail(data: any): Observable<any> {
    return this._authService.put(
      `${environment.baseUrl}/v1/Member/ResendVerifyEmail`,
      data
    ).pipe(map(res => res));
  }
  forgotPassword(data: any): Observable<any> {
    return this._authService.put(
      `${environment.baseUrl}/v1/Member/ForgotPassword`,
      data
    ).pipe(map(res => res));
  }
  setPassword(data: any): Observable<any> {
    return this._authService.put(
      `${environment.baseUrl}/v1/Member/ResetPassword`,
      data
    ).pipe(map(res => res));
  }
  login(data: any): Observable<any> {
    return this._authService.post(
      `${environment.baseUrl}/v1/oAuth/Login`,
      data,
      this._authService.getHeader(['application/x-www-form-urlencoded'])
    ).pipe(map(res => res));
  }
}
