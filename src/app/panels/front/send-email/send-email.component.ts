import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import { Router } from "@angular/router";
import { AuthService } from "src/app/utils/services/auth.service";
import { FrontService } from "../front.service";
import { NotificationService } from "src/app/utils/services/notification.service";
import { EmailValidator } from "src/app/utils/services/custom.validators";
import { finalize } from 'rxjs/operators';

@Component({
  selector: "app-send-email",
  templateUrl: "./send-email.component.html",
  styleUrls: ["./send-email.component.css"],
})
export class SendEmailComponent implements OnInit {
  public sendEmailForm: FormGroup = new FormGroup({
    userEmail: new FormControl(null, [Validators.required, Validators.email, Validators.minLength(2), Validators.maxLength(100), EmailValidator])
  });
  public isProcessing: boolean;
  public isResend: boolean;

  constructor(
    private _router: Router,
    private _authSevice: AuthService,
    private _notificationSevice: NotificationService,
    private _frontSevice: FrontService
  ) { }

  ngOnInit() {
    this._authSevice.clearStorage();
    if (this._router.url == "/Resend-Email") {
      this.isResend = true;
    } else {
      this.isResend = false;
    }
  }

  sendEmail() {
    if (this.sendEmailForm.valid) {
      try {
        this.isProcessing = true;
        let formData = {
          email: this.sendEmailForm.get("userEmail").value
        }

        if (this.isResend) {
          this._frontSevice.resendEmail(formData)
            .pipe(
              finalize(() => {
                this.isProcessing = false;
              })
            ).subscribe((apiResponse) => {
              this.isProcessing = false;
            });
        } else {
          this._frontSevice.forgotPassword(formData)
            .pipe(
              finalize(() => {
                this.isProcessing = false;
              })
            )
            .subscribe((apiResponse) => {
              this.isProcessing = false;
              this.sendEmailForm.reset();
            });
        }
      } catch (err) {
        this.isProcessing = false;
      }
    }
  }
}
