import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
} from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { AuthService } from "src/app/utils/services/auth.service";
import { NotificationService } from "src/app/utils/services/notification.service";
import { FrontService } from "../front.service";
import { EncryptionService } from "src/app/utils/services/encryption.service";
import {
  EmailValidator,
  PasswordValidator,
} from "src/app/utils/services/custom.validators";
import { finalize } from 'rxjs/operators';

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup = new FormGroup({
    userEmail: new FormControl(null, [Validators.required, Validators.email, Validators.minLength(2), Validators.maxLength(100), EmailValidator]),
    userPassword: new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(32), PasswordValidator])
  });

  public isProcessing: boolean;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _authSevice: AuthService,
    private _notificationSevice: NotificationService,
    private _encryptionSevice: EncryptionService,
    private _frontSevice: FrontService
  ) { }

  ngOnInit() {
    this._authSevice.clearStorage();
  }

  login() {
    if (this.loginForm.valid) {
      try {
        this.isProcessing = true;
        let formData = new URLSearchParams();
        formData.set("grant_type", "password");
        formData.set("token", this.loginForm.get("userEmail").value);
        formData.set("password", this.loginForm.get("userPassword").value);
        formData.set("deviceType", "web");
        formData.set("deviceToken", " ");
        this._frontSevice.login(formData.toString())
          .pipe(
            finalize(() => {
              this.isProcessing = false;
            })
          ).subscribe((apiResponse) => {
            this.isProcessing = false;
            this.loginForm.reset();
            this._authSevice.setValueToSessionStorage("data", JSON.stringify(apiResponse));
            this._router.navigate(["/App/Admin/Dashboard"]);
          });
      } catch (err) {
        this.isProcessing = false;
      }
    }
  }
}
