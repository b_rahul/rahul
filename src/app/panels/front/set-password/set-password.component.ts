import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { AuthService } from "src/app/utils/services/auth.service";
import { FrontService } from "../front.service";
import { NotificationService } from "src/app/utils/services/notification.service";
import { PasswordValidator, PasswordValidation } from "src/app/utils/services/custom.validators";
import { EncryptionService } from "src/app/utils/services/encryption.service";
import { finalize } from 'rxjs/operators';

@Component({
  selector: "app-set-password",
  templateUrl: "./set-password.component.html",
  styleUrls: ["./set-password.component.css"],
})
export class SetPasswordComponent implements OnInit {
  public setPasswordForm: FormGroup = new FormGroup({
    newPassword: new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(32), PasswordValidator]),
    confirmPassword: new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(32), PasswordValidator])
  });

  public isProcessing: boolean;
  public userEmail: string;
  public passwordError: string;
  private memberID: any;
  private token: any;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _authSevice: AuthService,
    private _notificationSevice: NotificationService,
    private _encryptionSevice: EncryptionService,
    private _frontSevice: FrontService
  ) { }

  ngOnInit() {
    this._authSevice.clearStorage();
    this._route.params.subscribe((params) => {
      this.memberID = params["MemberID"];
      this.token = params["Token"];
    });
  }

  setPassword() {
    if (this.setPasswordForm.valid) {
      try {
        this.passwordError = "";
        if (this.setPasswordForm.get("newPassword").value != this.setPasswordForm.get("confirmPassword").value) {
          this.passwordError = "Password and Confirm password doesn't match";
          return;
        } else if (!PasswordValidation(this.setPasswordForm.get("newPassword").value)
        ) {
          this.passwordError =
            "Password length should be 6-32characters, contain 1 Uppercase 1 Lowercase, 1 Number, 1 Special Character";
          return;
        } else {
          this.isProcessing = true;
          let formData = {
            token: this.token,
            id: this.memberID,
            password: this.setPasswordForm.get("newPassword").value
          }

          this._frontSevice
            .setPassword(formData)
            .pipe(
              finalize(() => {
                this.isProcessing = false;
              })
            )
            .subscribe((apiResponse) => {
              this.isProcessing = false;
              this._router.navigate(["/Login"]);
            });
        }
      } catch (err) {
        this.isProcessing = false;
      }
    }
  }
}
