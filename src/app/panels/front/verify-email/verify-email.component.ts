import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
import { AuthService } from "src/app/utils/services/auth.service";
import { NotificationService } from "src/app/utils/services/notification.service";
import { FrontService } from "../front.service";

@Component({
  selector: "app-verify-email",
  templateUrl: "./verify-email.component.html",
  styleUrls: ["./verify-email.component.css"],
})
export class VerifyEmailComponent implements OnInit {
  private memberID: string;
  private token: string;
  public verificationFailed: boolean = false;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _authSevice: AuthService,
    private _frontSevice: FrontService
  ) { }

  ngOnInit(): void {
    this._authSevice.clearStorage();
    this._route.params.subscribe((params) => {
      this.token = params["Token"];
      this.memberID = params["MemberID"];
    });
    this.verifyEmail();
  }

  verifyEmail() {
    if (!this.memberID || !this.token) {
      this._router.navigate(["/Login"]);
    } else {
      try {
        let formData = {
          token: this.token,
          id: this.memberID
        }

        this._frontSevice
          .verifyEmail(formData)
          .subscribe((apiResponse) => {
            this._router.navigate(["/Set-Password", this.token, this.memberID]);
          });
      } catch (err) { }
    }
  }
}
