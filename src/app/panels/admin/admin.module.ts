import { NgModule } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";

import { AppMaterialModule } from "src/app/app-material-modules";
import { SharedModule } from "../shared/shared.module";

import { MainComponent } from "../shared/main/main.component";
import { MainModule } from "../shared/main/main.module";
import { AuthGuard } from 'src/app/utils/guards/auth.guard';
import { ProfileComponent } from './profile/profile.component';
import { DataTablesModule } from 'angular-datatables';

const AdminRoutes: Routes = [
  {
    path: "",
    component: MainComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: "Dashboard",
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: "Banner",
        loadChildren: () => import('./banner/banner.module').then(m => m.BannerModule)
      },
      {
        path: "Category",
        loadChildren: () => import('./category/category.module').then(m => m.CategoryModule)
      },
      {
        path: "Product",
        loadChildren: () => import('./product/product.module').then(m => m.ProductModule)
      },
      {
        path: "User",
        loadChildren: () => import('./user/user.module').then(m => m.UserModule)
      },
      {
        path: "Profile",
        component: ProfileComponent,
      },
      {
        path: "",
        redirectTo: "Dashboard",
        pathMatch: "full",
      },
    ]
  }
];

@NgModule({  
  declarations: [ProfileComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    AppMaterialModule,
    FlexLayoutModule,
    DataTablesModule,
    MainModule,
    RouterModule.forChild(AdminRoutes),
  ],
  providers: [],
})
export class AdminModule {}
