import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AddProductComponent } from '../../../product/add-product/add-product.component';
import { ProductService } from '../../../product/product.service';
import { DecimalValidator, NumericValidator, FormErrorMessage } from 'src/app/utils/services/custom.validators';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.css']
})
export class ViewUserComponent implements OnInit {

  public addUpdateProductForm: FormGroup ;

  public isProcessing: boolean;
  public isSubmitted: boolean;
  public productError: string;
  public productImages: File[];
  private allowedFormats = ['image/jpeg', 'image/jpg', 'image/png'];
  public action: string;
  public categories = [];

  constructor(
    public dialogRef: MatDialogRef<AddProductComponent>,
    @Inject(MAT_DIALOG_DATA) public productRef: any,
    private _productService: ProductService  ) { }

  ngOnInit(): void {
    let disable = false;
    if (this.productRef) {
      this.action = this.productRef.action;
      if (this.action == 'view') disable = true;

      this.addUpdateProductForm = new FormGroup({
        productCategory: new FormControl({ value: this.productRef.categoryId, disabled: disable }, [Validators.required, Validators.minLength(2), Validators.maxLength(100)]),
        productName: new FormControl({ value: this.productRef.title, disabled: disable }, [Validators.required, Validators.minLength(2), Validators.maxLength(100)]),
        productPrice: new FormControl({ value: this.productRef.price, disabled: disable }, [Validators.required, DecimalValidator]),
        productDiscount: new FormControl({ value: this.productRef.discount, disabled: disable }, [DecimalValidator]),
        productSellingPrice: new FormControl({ value: this.productRef.salePrice, disabled: disable }, [Validators.required, DecimalValidator]),
        productQuantity: new FormControl({ value: this.productRef.info.quantity, disabled: disable }, [Validators.required, Validators.maxLength(100), NumericValidator]),
        productColor: new FormControl({ value: this.productRef.info.color, disabled: disable }, [Validators.maxLength(100)]),
        productDescription: new FormControl({ value: this.productRef.info.desc, disabled: disable }, [Validators.required])
      });

    }
    else {
      this.action = 'add';

      this.addUpdateProductForm = new FormGroup({
        productCategory: new FormControl(null, [Validators.required, Validators.minLength(2), Validators.maxLength(100)]),
        productName: new FormControl(null, [Validators.required, Validators.minLength(2), Validators.maxLength(100)]),
        productPrice: new FormControl(null, [Validators.required, DecimalValidator]),
        productDiscount: new FormControl(null, [DecimalValidator]),
        productSellingPrice: new FormControl(null, [Validators.required, DecimalValidator]),
        productQuantity: new FormControl(null, [Validators.required, Validators.maxLength(100), NumericValidator]),
        productColor: new FormControl(null, [Validators.maxLength(100)]),
        productDescription: new FormControl(null, [Validators.required])
      });
    }

    this.getCategories();
  }

  /** Form Error Messages */
  getErrorMessage(control: string) {
    if (this.isSubmitted)
      return FormErrorMessage(this.addUpdateProductForm, control);
  }

  getCategories() {
    this._productService.getCategories()
      .subscribe((apiResponse) => {
        this.categories = apiResponse;
      });
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

  addUpdateProduct() {
    this.isSubmitted = true;
    if (this.addUpdateProductForm.valid) {
      try {

        let formData = {
          categoryId: this.addUpdateProductForm.get("productCategory").value,
          title: this.addUpdateProductForm.get("productName").value,
          price: this.addUpdateProductForm.get("productPrice").value,
          discount: this.addUpdateProductForm.get("productDiscount").value,
          salePrice: this.addUpdateProductForm.get("productSellingPrice").value,
          info: {
            quantity: this.addUpdateProductForm.get('productQuantity').value,
            color: this.addUpdateProductForm.get('productColor').value,
            desc: this.addUpdateProductForm.get('productDescription').value,
          }
        }

        if (this.productRef) {

          let validateStatus = true;

          if (this.productImages) {
            for (let i = 0; i < this.productImages.length; i++) {
              let file = this.productImages[i];
              if (!this.allowedFormats.includes(file.type)) {
                this.productError = "Please upload Product file JPEG/JPG/PNG format only";
                validateStatus = false;
                return;
              }
              else if (file.size > 5000000) {
                this.productError = "Please upload Product file less than 5 MB.";
                validateStatus = false;
                return;
              }
              else {
                validateStatus = true;
              }
            }
          }

          if (!validateStatus) return false;

          this.isProcessing = true;
          this._productService.updateProduct(formData, this.productRef.id)
            .pipe(
              finalize(() => {
                this.isProcessing = false;
              })
            ).subscribe(async () => {
              this.isProcessing = false;

              if (this.productImages) {
                for (let i = 0; i < this.productImages.length; i++) {
                  let file = this.productImages[i];
                  await this.addProductImage(file, this.productRef.id);
                }
              }

              this.dialogRef.close(true);
            });
        }
        else {
          this.isProcessing = true;
          this._productService.addProduct(formData)
            .pipe(
              finalize(() => {
                this.isProcessing = false;
              })
            ).subscribe(() => {
              this.isProcessing = false;
              this.dialogRef.close(true);
            });
        }


      } catch (err) {
        this.isProcessing = false;
      }
    }
  }

  onFileSelect(event) {
    console.log(event.target.files)
    if (event.target.files.length > 0) {
      this.productImages = event.target.files;
    }
  }

  async addProductImage(file: File, productID: string) {
    try {
      this.isProcessing = true;
      let formData = new FormData();
      formData.set("image", file, file.name);
      await this._productService.addProductImage(formData, productID)
        .toPromise()
        .then(() => {
          this.isProcessing = false
        })
        .catch(() => {
          this.isProcessing = false;
        })
    } catch (err) {
      this.isProcessing = false;
    }
  }

  deleteProductImage(imageID: string) {
    try {
      this.isProcessing = true;
      this._productService.deleteProductImage(imageID)
        .pipe(
          finalize(() => {
            this.isProcessing = false;
          })
        ).subscribe(() => {
          this.isProcessing = false;
          this.dialogRef.close(true);
        });
    } catch (err) {
      this.isProcessing = false;
    }
  }
}
