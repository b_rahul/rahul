import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { UserService } from '../../user/user.service';
import { MatDialog } from '@angular/material/dialog';
import Swal from 'sweetalert2';
import { finalize } from 'rxjs/operators';
import * as _ from "lodash";
import { ViewUserComponent } from './view-user/view-user.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  public dataColumns: string[] = ['index', 'username', 'firstName', 'lastName', 'fbId', 'googleId', 'email', 'contactNumber', 'roles', 'status', 'actions'];
  public dataSource = new MatTableDataSource();

  public isProcessing: boolean;
  public isUsersLoading: boolean;

  public users = [];
  public categories = [];
  public tableOptions = {
    total: 0,
    pageIndex: 0,
    previousPageIndex: 0,
    pageSize: 10
  };

  public pageEvent: PageEvent;
  public selectedFilter: string = 'buyer';

  constructor(
    private _userService: UserService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.getUsers(this.selectedFilter);
  }

  getUsers(filterBy: string, searchValue?: string) {
    this.isUsersLoading = true;
    this._userService.getUsers(this.tableOptions, filterBy, searchValue)
      .subscribe((apiResponse) => {
        this.users = apiResponse.members;
        this.tableOptions = this.generateOptions(apiResponse.totalCount)
        this.isUsersLoading = false;

        /** DATATABLE */
        this.dataSource = new MatTableDataSource(this.users);
        this.dataSource.sort = this.sort;
        /** DATATABLE */
      });
  }

  openDialog(element?: any) {
    let dialogRef = this.dialog.open(ViewUserComponent, { data: element });

    dialogRef.afterClosed().subscribe(result => {
      if (result) this.getUsers(this.selectedFilter);
    });
  }

  editViewUser(action: string, editID: string) {
    if (editID != null) {
      let category = _.find(this.users, { id: editID })
      category.action = action;
      this.openDialog(category);
    }
  }

  searchUser(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.getUsers(this.selectedFilter, filterValue.trim().toLowerCase());
  }

  paginateUsers(event?: PageEvent) {
    this.isProcessing = true;

    this.tableOptions.pageIndex = event.pageIndex;
    this.tableOptions.previousPageIndex = event.previousPageIndex;
    this.tableOptions.pageSize = event.pageSize;
    this.getUsers(this.selectedFilter);
    return event;
  }

  generateOptions(totalCount: any) {
    return {
      total: totalCount,
      pageIndex: (totalCount <= this.tableOptions.pageSize) ? 0 : this.tableOptions.pageIndex,
      previousPageIndex: (totalCount <= this.tableOptions.pageSize) ? 0 : this.tableOptions.previousPageIndex,
      pageSize: this.tableOptions.pageSize
    };
  }

  enableDisableUser(action: string, userID: string) {
    Swal.fire({
      title: 'Are you sure?',
      text: 'To ' + action + ' user!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, ' + action + ' it!'
    }).then((result) => {
      if (result.value) {

        try {
          this.isProcessing = true;
          let formData = {
            action: action.toLowerCase(),
            memberId: userID
          }
          this._userService.enableDisableUser(formData)
            .pipe(
              finalize(() => {
                this.isProcessing = false;
              })
            ).subscribe(() => {
              this.isProcessing = false;
              this.getUsers(this.selectedFilter);
            });
        } catch (err) {
          this.isProcessing = false;
        }
      }
    })
  }
}
