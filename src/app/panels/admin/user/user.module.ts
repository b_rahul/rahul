import { NgModule } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";

import { AppMaterialModule } from "src/app/app-material-modules";
import { SharedModule } from "../../shared/shared.module";

import { DataTablesModule } from 'angular-datatables';
import { UsersComponent } from './users/users.component';
import { ViewUserComponent } from './users/view-user/view-user.component';

const UserRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "",
        redirectTo: "Manage",
        pathMatch: "full",
      },
      {
        path: "Manage",
        component: UsersComponent,
      }
    ],
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    AppMaterialModule,
    FlexLayoutModule,
    DataTablesModule,
    RouterModule.forChild(UserRoutes),
  ],
  declarations: [UsersComponent, ViewUserComponent],
  providers: [],
})
export class UserModule {}
