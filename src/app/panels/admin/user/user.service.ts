import { Injectable } from "@angular/core";
import { AuthService } from "src/app/utils/services/auth.service";
import { environment } from "src/environments/environment";
import { Observable } from "rxjs";
import { map } from 'rxjs/operators';
import { HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: "root",
})
export class UserService {
  constructor(private _authService: AuthService) { }

  /** USER START */
  getUsers(tableOptions: any, filterBy: string, searchValue?: string): Observable<any> {

    const recordsToSkip = tableOptions.pageIndex == 0 ? 0 : (tableOptions.pageIndex * tableOptions.pageSize);
    let params = new HttpParams();
    params = params.append('offset', recordsToSkip.toString());
    params = params.append('limit', tableOptions.pageSize.toString());
    params = params.append('role', filterBy);

    let requestUrl = `${environment.baseUrl}/v1/Member/All`;
    if (searchValue) {
      requestUrl = `${environment.baseUrl}/v1/Member/Search`;
      params = params.append('name', searchValue);
    }
    
    return this._authService.get(
      requestUrl,
      this._authService.getHeader(['application/json']),
      params
    ).pipe(map(res => res));
  }

  enableDisableUser(data: any): Observable<any> {
    return this._authService.post(
      `${environment.baseUrl}/v1/Member/DisableOrEnable`,
      data
    ).pipe(map(res => res));
  }

  /** USER END */

}
