import { Component, OnInit, ViewChild } from '@angular/core';
import { finalize } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ProductService } from '../product.service';
import { MatDialog } from '@angular/material/dialog';
import { AddProductComponent } from '../add-product/add-product.component';
import * as _ from "lodash";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  public dataColumns: string[] = ['index', 'image', 'title', 'price', 'salePrice', 'discount', 'totalLikes', 'actions'];
  public dataSource = new MatTableDataSource();

  public isProcessing: boolean;
  public isProductsLoading: boolean;

  public products = [];
  public categories = [];
  public tableOptions = {
    total: 0,
    pageIndex: 0,
    previousPageIndex: 0,
    pageSize: 10
  };

  public pageEvent: PageEvent;

  constructor(
    private _productService: ProductService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.getProducts('None');
    this.getCategories();
  }

  getCategories() {
    this._productService.getCategories()
      .subscribe((apiResponse) => {
        this.categories = apiResponse;
      });
  }

  getProducts(filterBy: string, categoryID?: string) {
    this.isProductsLoading = true;
    this._productService.getProducts(this.tableOptions, filterBy, categoryID)
      .subscribe((apiResponse) => {
        this.products = apiResponse.products;
        this.tableOptions = this.generateOptions(apiResponse.totalCount)
        this.products.forEach(product => {
          if (product.images.length > 0) {
            product.path = "//" + product.images[0].path;
          }
          else {
            product.path = 'assets/images/no-image.png';
          }
        })
        this.isProductsLoading = false;

        /** DATATABLE */
        this.dataSource = new MatTableDataSource(this.products);
        this.dataSource.sort = this.sort;
        /** DATATABLE */
      });
  }

  openDialog(element?: any) {
    let dialogRef = this.dialog.open(AddProductComponent, { data: element });

    dialogRef.afterClosed().subscribe(result => {
      if (result) this.getProducts('None');
    });
  }

  editViewProduct(action: string, editID: string) {
    if (editID != null) {
      let category = _.find(this.products, { id: editID })
      category.action = action;
      this.openDialog(category);
    }
  }

  searchProduct(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.getProducts('search', filterValue.trim().toLowerCase());
  }

  paginateProducts(event?: PageEvent) {
    this.isProcessing = true;

    this.tableOptions.pageIndex = event.pageIndex;
    this.tableOptions.previousPageIndex = event.previousPageIndex;
    this.tableOptions.pageSize = event.pageSize;
    this.getProducts('None');
    return event;
  }

  generateOptions(totalCount: any) {
    return {
      total: totalCount,
      pageIndex: (totalCount <= this.tableOptions.pageSize) ? 0 : this.tableOptions.pageIndex,
      previousPageIndex: (totalCount <= this.tableOptions.pageSize) ? 0 : this.tableOptions.previousPageIndex,
      pageSize: this.tableOptions.pageSize
    };
  }

  deleteProduct(productID: string) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {

        try {
          this.isProcessing = true;
          this._productService.deleteProduct(productID)
            .pipe(
              finalize(() => {
                this.isProcessing = false;
              })
            ).subscribe(() => {
              this.isProcessing = false;
              this.getProducts('None');
            });
        } catch (err) {
          this.isProcessing = false;
        }
      }
    })
  }
}
