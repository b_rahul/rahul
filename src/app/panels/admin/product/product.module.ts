import { NgModule } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";

import { AppMaterialModule } from "src/app/app-material-modules";
import { SharedModule } from "../../shared/shared.module";

import { DataTablesModule } from 'angular-datatables';
import { AddProductComponent } from './add-product/add-product.component';
import { ProductsComponent } from './products/products.component';

const ProductRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "",
        redirectTo: "Manage",
        pathMatch: "full",
      },
      {
        path: "Add-Product",
        component: AddProductComponent,
      },
      {
        path: "Manage",
        component: ProductsComponent,
      }
    ],
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    AppMaterialModule,
    FlexLayoutModule,
    DataTablesModule,
    RouterModule.forChild(ProductRoutes),
  ],
  declarations: [AddProductComponent, ProductsComponent],
  providers: [],
})
export class ProductModule {}
