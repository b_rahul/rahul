import { Injectable } from "@angular/core";
import { AuthService } from "src/app/utils/services/auth.service";
import { environment } from "src/environments/environment";
import { Observable } from "rxjs";
import { map } from 'rxjs/operators';
import { HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: "root",
})
export class ProductService {
  constructor(private _authService: AuthService) { }

  /** CATEGORY START */
  getCategories(data?: any): Observable<any> {
    return this._authService.get(
      `${environment.baseUrl}/v1/Category`,
      this._authService.getHeader(['application/json'])
    ).pipe(map(res => res));
  }

  addCategory(data: any): Observable<any> {
    return this._authService.post(
      `${environment.baseUrl}/v1/Category`,
      data
    ).pipe(map(res => res));
  }

  updateCategory(data: any, categoryID: string): Observable<any> {
    return this._authService.put(
      `${environment.baseUrl}/v1/Category/` + categoryID,
      data
    ).pipe(map(res => res));
  }

  deleteCategory(data: any): Observable<any> {
    return this._authService.delete(
      `${environment.baseUrl}/v1/Category/` + data
    ).pipe(map(res => res));
  }

  addCategoryImage(data: any, categoryID: string): Observable<any> {
    return this._authService.post(
      `${environment.baseUrl}/v1/Category/Image/` + categoryID,
      data
    ).pipe(map(res => res));
  }

  deleteCategoryImage(data: any): Observable<any> {
    return this._authService.delete(
      `${environment.baseUrl}/v1/Category/Image/getById/` + data
    ).pipe(map(res => res));
  }
  /** CATEGORY END */

  /** PRODUCTS START */
  getProducts(tableOptions: any, filterBy: string, value: string): Observable<any> {
    let requestUrl = `${environment.baseUrl}/v1/Product`;
    if (filterBy == 'category' && value) requestUrl = `${environment.baseUrl}/v1/Product/Category/` + value
    if (filterBy == 'search' && value) requestUrl = `${environment.baseUrl}/v1/Product/Search/` + value

    const recordsToSkip = tableOptions.pageIndex == 0 ? 0 : (tableOptions.pageIndex * tableOptions.pageSize);
    let params = new HttpParams();
    params = params.append('offset', recordsToSkip.toString());
    params = params.append('limit', tableOptions.pageSize.toString());
    
    return this._authService.get(
      requestUrl,
      this._authService.getHeader(['application/json']),
      params
    ).pipe(map(res => res));
  }

  addProduct(data: any): Observable<any> {
    return this._authService.post(
      `${environment.baseUrl}/v1/Product`,
      data
    ).pipe(map(res => res));
  }

  updateProduct(data: any, ProductID: string): Observable<any> {
    return this._authService.put(
      `${environment.baseUrl}/v1/Product/` + ProductID,
      data
    ).pipe(map(res => res));
  }

  deleteProduct(data: any): Observable<any> {
    return this._authService.delete(
      `${environment.baseUrl}/v1/Product/` + data
    ).pipe(map(res => res));
  }

  addProductImage(data: any, ProductID: string): Observable<any> {
    return this._authService.post(
      `${environment.baseUrl}/v1/Product/Image/` + ProductID,
      data
    ).pipe(map(res => res));
  }

  deleteProductImage(data: any): Observable<any> {
    return this._authService.delete(
      `${environment.baseUrl}/v1/Product/Image/getById/` + data
    ).pipe(map(res => res));
  }
  /** PRODUCTS END */

}
