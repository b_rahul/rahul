import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { BannerService } from '../banner.service';
import { finalize } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { MatDialog } from '@angular/material/dialog';
import { AddBannerComponent } from '../add-banner/add-banner.component';

@Component({
  selector: 'app-banners',
  templateUrl: './banners.component.html',
  styleUrls: ['./banners.component.css']
})
export class BannersComponent implements OnInit {

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  public dataColumns: string[] = ['index', 'image', 'name', 'type', 'actions'];
  public dataSource = new MatTableDataSource();

  public isProcessing: boolean;
  public isBannersLoading: boolean;
  public banners = [];
  // public toggleInputForm: boolean = false;
  // public bannerError: string;

  // private bannerImage: File;
  // private allowedFormats = ['image/jpeg', 'image/jpg', 'image/png'];

  constructor(
    private _bannerService: BannerService,
    public dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.getBanners();
  }

  getBanners() {
    this.isBannersLoading = true;
    this._bannerService.getBanners()
      .subscribe((apiResponse) => {
        this.banners = apiResponse;
        this.banners.forEach(banner => {
          banner.path = "//" + banner.path
        })
        this.isBannersLoading = false;

        /** DATATABLE */
        this.dataSource = new MatTableDataSource(this.banners);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        /** DATATABLE */
      });
  }

  deleteBanner(bannerID: string) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {

        try {
          this.isProcessing = true;
          this._bannerService.deleteBanner(bannerID)
            .pipe(
              finalize(() => {
                this.isProcessing = false;
              })
            ).subscribe(() => {
              this.isProcessing = false;
              this.getBanners();
            });
        } catch (err) {
          this.isProcessing = false;
        }
      }
    })
  }
  
  openDialog(element?: any) {
    let dialogRef = this.dialog.open(AddBannerComponent, { data: element });

    dialogRef.afterClosed().subscribe(result => {
      if (result) this.getBanners();
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
