import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { finalize } from 'rxjs/operators';
import { BannerService } from '../banner.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AlphaNumericValidator, FormErrorMessage } from 'src/app/utils/services/custom.validators';

@Component({
  selector: 'app-add-banner',
  templateUrl: './add-banner.component.html',
  styleUrls: ['./add-banner.component.css']
})
export class AddBannerComponent implements OnInit {
  public addBannerForm: FormGroup = new FormGroup({
    bannerCategory: new FormControl(null, [Validators.required, Validators.minLength(2), Validators.maxLength(100)]),
    bannerName: new FormControl(null, [Validators.required, Validators.minLength(2), Validators.maxLength(100), AlphaNumericValidator]),
    bannerType: new FormControl(null, [Validators.required, Validators.minLength(2), Validators.maxLength(100), AlphaNumericValidator])
  });

  public isProcessing: boolean;
  public isSubmitted: boolean;
  public bannerError: string;
  public categories: any;

  private bannerImage: File;
  private iconImage: File;

  private allowedFormats = ['image/jpeg', 'image/jpg', 'image/png'];


  constructor(
    public dialogRef: MatDialogRef<AddBannerComponent>,
    @Inject(MAT_DIALOG_DATA) public bannerRef: any,
    private _bannerService: BannerService  ) { }

  ngOnInit() {
    this.getCategories();
  }

  /** Form Error Messages */
  getErrorMessage(control: string) {
    if (this.isSubmitted)
      return FormErrorMessage(this.addBannerForm, control);
  }

  onFileSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.bannerImage = file;
    }
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

  getCategories() {
    this._bannerService.getCategories()
      .subscribe((apiResponse) => {
        this.categories = apiResponse;
      });
  }

  addBanner() {
    this.isSubmitted = true;
    if (this.addBannerForm.valid) {
      try {
        if (!this.allowedFormats.includes(this.bannerImage.type) || (this.addBannerForm.get('isNavigation').value && !this.allowedFormats.includes(this.bannerImage.type))) {
          this.bannerError = "Please upload JPEG/JPG/PNG format only";
        }
        else if (this.bannerImage.size > 5000000 || (this.addBannerForm.get('isNavigation').value && this.iconImage.size > 5000000)) {
          this.bannerError = "Please upload file less than 5 MB.";
        }
        else {
          this.isProcessing = true;
          let formData = new FormData();
          formData.set("categoryId", this.addBannerForm.get("bannerCategory").value);
          formData.set("name", this.addBannerForm.get("bannerName").value);
          formData.set("type", this.addBannerForm.get("bannerType").value);
          formData.set("banner", this.bannerImage, this.bannerImage.name);
          this._bannerService.addBanner(formData)
            .pipe(
              finalize(() => {
                this.isProcessing = false;
              })
            ).subscribe(() => {
              this.isProcessing = false;
              this.addBannerForm.reset();
              this.dialogRef.close(true);
            });
        }
      } catch (err) {
        this.isProcessing = false;
      }
    }
  }
}
