import { NgModule } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";

import { AppMaterialModule } from "src/app/app-material-modules";
import { SharedModule } from "../../shared/shared.module";

import { DataTablesModule } from 'angular-datatables';
import { AddBannerComponent } from './add-banner/add-banner.component';
import { BannersComponent } from './banners/banners.component';

const BannerRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "",
        redirectTo: "Manage",
        pathMatch: "full",
      },
      {
        path: "Add-Banner",
        component: AddBannerComponent,
      },
      {
        path: "Manage",
        component: BannersComponent,
      }
    ],
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    AppMaterialModule,
    FlexLayoutModule,
    DataTablesModule,
    RouterModule.forChild(BannerRoutes),
  ],
  declarations: [AddBannerComponent, BannersComponent],
  providers: [],
})
export class BannerModule {}
