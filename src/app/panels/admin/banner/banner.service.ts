import { Injectable } from "@angular/core";
import { AuthService } from "src/app/utils/services/auth.service";
import { environment } from "src/environments/environment";
import { Observable } from "rxjs";
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: "root",
})
export class BannerService {
  constructor(private _authService: AuthService) { }

  /** BANNER START */
  getBanners(data?: any): Observable<any> {
    return this._authService.get(
      `${environment.baseUrl}/v1/Member/Banner`,
      this._authService.getHeader(['application/json'])
    ).pipe(map(res => res));
  }

  addBanner(data: any): Observable<any> {
    return this._authService.post(
      `${environment.baseUrl}/v1/Member/Banner`,
      data
    ).pipe(map(res => res));
  }

  deleteBanner(data: any): Observable<any> {
    return this._authService.delete(
      `${environment.baseUrl}/v1/Member/Banner/` + data
    ).pipe(map(res => res));
  }

  /** BANNER END */

  /** CATEGORY START */
  getCategories(data?: any): Observable<any> {
    return this._authService.get(
      `${environment.baseUrl}/v1/Category`,
      this._authService.getHeader(['application/json'])
    ).pipe(map(res => res));
  }
  /** CATEGORY END */

}
