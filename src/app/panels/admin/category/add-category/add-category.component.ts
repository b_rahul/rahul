import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AlphaNumericValidator, FormErrorMessage } from 'src/app/utils/services/custom.validators';
import { finalize } from 'rxjs/operators';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CategoryService } from '../category.service';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.css']
})
export class AddCategoryComponent implements OnInit {
  public addUpdateCategoryForm: FormGroup;

  public isProcessing: boolean;
  public isSubmitted: boolean;
  public categoryError: string;
  public categoryImages: File[];
  public iconImage: File;
  private allowedFormats = ['image/jpeg', 'image/jpg', 'image/png'];
  public action: string;

  constructor(
    public dialogRef: MatDialogRef<AddCategoryComponent>,
    @Inject(MAT_DIALOG_DATA) public categoryRef: any,
    private _categoryService: CategoryService  ) { }

  ngOnInit(): void {
    let disable = false;
    if (this.categoryRef) {
      this.action = this.categoryRef.action;
      if (this.action == 'view') disable = true;

      this.addUpdateCategoryForm = new FormGroup({
        categoryName: new FormControl({ value: this.categoryRef.name, disabled: disable }, [Validators.required, Validators.minLength(2), Validators.maxLength(100), AlphaNumericValidator]),
        categoryType: new FormControl({ value: this.categoryRef.type, disabled: disable }, [Validators.required, Validators.minLength(2), Validators.maxLength(100), AlphaNumericValidator]),
        isNavigation: new FormControl({ value: this.categoryRef.isNavigation, disabled: disable })
      });

    }
    else {
      this.action = 'add';

      this.addUpdateCategoryForm = new FormGroup({
        categoryName: new FormControl(null, [Validators.required, Validators.minLength(2), Validators.maxLength(100), AlphaNumericValidator]),
        categoryType: new FormControl(null, [Validators.required, Validators.minLength(2), Validators.maxLength(100), AlphaNumericValidator]),
        isNavigation: new FormControl(null)
      });
    }
  }

  /** Form Error Messages */
  getErrorMessage(control: string) {
    if (this.isSubmitted)
      return FormErrorMessage(this.addUpdateCategoryForm, control);
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

  addUpdateCategory() {
    this.isSubmitted = true;
    if (this.addUpdateCategoryForm.valid) {
      try {
        let formData = {
          name: this.addUpdateCategoryForm.get("categoryName").value,
          type: this.addUpdateCategoryForm.get("categoryType").value,
          isNavigation: this.addUpdateCategoryForm.get("isNavigation").value
        }

        if (this.categoryRef) {

          let validateStatus = true;

          if (this.categoryImages) {
            for (let i = 0; i < this.categoryImages.length; i++) {
              let file = this.categoryImages[i];
              if (!this.allowedFormats.includes(file.type)) {
                this.categoryError = "Please upload Category file JPEG/JPG/PNG format only";
                validateStatus = false;
                return;
              }
              else if (file.size > 5000000) {
                this.categoryError = "Please upload Category file less than 5 MB.";
                validateStatus = false;
                return;
              }
              else {
                validateStatus = true;
              }
            }
          }
          if (this.addUpdateCategoryForm.get('isNavigation').value && this.iconImage) {
            if (!this.allowedFormats.includes(this.iconImage.type)) {
              this.categoryError = "Please upload Icon file JPEG/JPG/PNG format only";
              validateStatus = false;
            }
            else if (this.iconImage.size > 5000000) {
              this.categoryError = "Please upload Icon file less than 5 MB.";
              validateStatus = false;
            }
            else {
              validateStatus = true;
            }
          }

          if (!validateStatus) return false;

          this.isProcessing = true;
          this._categoryService.updateCategory(formData, this.categoryRef.id)
            .pipe(
              finalize(() => {
                this.isProcessing = false;
              })
            ).subscribe(async () => {
              this.isProcessing = false;

              if (this.categoryImages) {
                for (let i = 0; i < this.categoryImages.length; i++) {
                  let file = this.categoryImages[i];
                  await this.addCategoryImage(file, this.categoryRef.id);
                }
              }
              if (this.iconImage) {
                this.addCategoryIcon(this.iconImage, this.categoryRef.id);
              }

              this.dialogRef.close(true);
            });
        }
        else {
          this.isProcessing = true;
          this._categoryService.addCategory(formData)
            .pipe(
              finalize(() => {
                this.isProcessing = false;
              })
            ).subscribe(() => {
              this.isProcessing = false;
              this.dialogRef.close(true);
            });
        }


      } catch (err) {
        this.isProcessing = false;
      }
    }
  }

  onFileSelect(event) {
    console.log(event.target.files)
    if (event.target.files.length > 0) {
      this.categoryImages = event.target.files;
    }
  }

  onIconSelect(event) {
    if (event.target.files.length > 0) {
      this.iconImage = event.target.files[0];
    }
  }

  async addCategoryImage(file: File, categoryID: string) {
    try {
      this.isProcessing = true;
      let formData = new FormData();
      formData.set("image", file, file.name);
      await this._categoryService.addCategoryImage(formData, categoryID)
        .toPromise()
        .then(() => {
          this.isProcessing = false
        })
        .catch(() => {
          this.isProcessing = false;
        })
    } catch (err) {
      this.isProcessing = false;
    }
  }

  async addCategoryIcon(file: File, categoryID: string) {
    try {
      this.isProcessing = true;
      let formData = new FormData();
      formData.set("image", file, file.name);
      await this._categoryService.addCategoryIcon(formData, categoryID)
        .toPromise()
        .then(() => {
          this.isProcessing = false
        })
        .catch(() => {
          this.isProcessing = false;
        })
    } catch (err) {
      this.isProcessing = false;
    }
  }

  deleteCategoryImage(imageID: string) {
    try {
      this.isProcessing = true;
      this._categoryService.deleteCategoryImage(imageID)
        .pipe(
          finalize(() => {
            this.isProcessing = false;
          })
        ).subscribe(() => {
          this.isProcessing = false;
          this.dialogRef.close(true);
        });
    } catch (err) {
      this.isProcessing = false;
    }
  }
}
