import { NgModule } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";

import { AppMaterialModule } from "src/app/app-material-modules";
import { SharedModule } from "../../shared/shared.module";

import { DataTablesModule } from 'angular-datatables';
import { AddCategoryComponent } from './add-category/add-category.component';
import { CategoriesComponent } from './categories/categories.component';

const CategoryRoutes: Routes = [
  {
    path: "",
    children: [
      {
        path: "",
        redirectTo: "Manage",
        pathMatch: "full",
      },
      {
        path: "Add-Category",
        component: AddCategoryComponent,
      },
      {
        path: "Manage",
        component: CategoriesComponent,
      }
    ],
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    AppMaterialModule,
    FlexLayoutModule,
    DataTablesModule,
    RouterModule.forChild(CategoryRoutes),
  ],
  declarations: [AddCategoryComponent, CategoriesComponent],
  providers: [],
})
export class CategoryModule {}
