import { Injectable } from "@angular/core";
import { AuthService } from "src/app/utils/services/auth.service";
import { environment } from "src/environments/environment";
import { Observable } from "rxjs";
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: "root",
})
export class CategoryService {
  constructor(private _authService: AuthService) { }

  /** CATEGORY START */
  getCategories(filterBy?: string): Observable<any> {
    let requestUrl = `${environment.baseUrl}/v1/Category`;
    if (filterBy) requestUrl = `${environment.baseUrl}/v1/Category/Featured/Category`
    return this._authService.get(
      requestUrl,
      this._authService.getHeader(['application/json'])
    ).pipe(map(res => res));
  }

  addCategory(data: any): Observable<any> {
    return this._authService.post(
      `${environment.baseUrl}/v1/Category`,
      data
    ).pipe(map(res => res));
  }

  updateCategory(data: any, categoryID: number): Observable<any> {
    return this._authService.put(
      `${environment.baseUrl}/v1/Category/` + categoryID,
      data
    ).pipe(map(res => res));
  }

  deleteCategory(data: any): Observable<any> {
    return this._authService.delete(
      `${environment.baseUrl}/v1/Category/` + data
    ).pipe(map(res => res));
  }

  addCategoryImage(data: any, categoryID: string): Observable<any> {
    return this._authService.post(
      `${environment.baseUrl}/v1/Category/Image/` + categoryID,
      data
    ).pipe(map(res => res));
  }

  addCategoryIcon(data: any, categoryID: string): Observable<any> {
    return this._authService.post(
      `${environment.baseUrl}/v1/Category/Logo/` + categoryID,
      data
    ).pipe(map(res => res));
  }

  deleteCategoryImage(data: any): Observable<any> {
    return this._authService.delete(
      `${environment.baseUrl}/v1/Category/Image/getById/` + data
    ).pipe(map(res => res));
  }
  /** CATEGORY END */

}
