import { Component, OnInit, ViewChild } from '@angular/core';
import { finalize } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { CategoryService } from '../../category/category.service';
import { MatDialog } from '@angular/material/dialog';
import { AddCategoryComponent } from '../add-category/add-category.component';
import * as _ from "lodash";

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  public dataColumns: string[] = ['index', 'image', 'title', 'type', 'actions'];
  public dataSource = new MatTableDataSource();

  public isProcessing: boolean;
  public isCategoriesLoading: boolean;
  public categories = [];

  constructor(
    private _categoryService: CategoryService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.getCategories();
  }

  getCategories(filterBy?: string) {
    this.isCategoriesLoading = true;
    this._categoryService.getCategories(filterBy)
      .subscribe((apiResponse) => {
        this.categories = apiResponse;
        this.categories.forEach(category => {
          if (category.images.length > 0) {
            category.path = "//" + category.images[0].path;
          }
          else {
            category.path = 'assets/images/no-image.png';
          }
        })
        this.isCategoriesLoading = false;

        /** DATATABLE */
        this.dataSource = new MatTableDataSource(this.categories);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        /** DATATABLE */
      });
  }

  openDialog(element?: any) {
    let dialogRef = this.dialog.open(AddCategoryComponent, { data: element });

    dialogRef.afterClosed().subscribe(result => {
      if (result) this.getCategories();
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  editViewCategory(action: string, editID: string) {
    if (editID != null) {
      let category = _.find(this.categories, { id: editID })
      category.action = action;
      this.openDialog(category);
    }
  }

  deleteCategory(categoryID: string) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {

        try {
          this.isProcessing = true;
          this._categoryService.deleteCategory(categoryID)
            .pipe(
              finalize(() => {
                this.isProcessing = false;
              })
            ).subscribe(() => {
              this.isProcessing = false;
              this.getCategories();
            });
        } catch (err) {
          this.isProcessing = false;
        }
      }
    })
  }
}
