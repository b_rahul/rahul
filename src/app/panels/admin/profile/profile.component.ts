import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/utils/services/auth.service';
import { NotificationService } from 'src/app/utils/services/notification.service';
import { EncryptionService } from 'src/app/utils/services/encryption.service';
import { finalize } from 'rxjs/operators';
import { AdminService } from '../admin.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PasswordValidator, EmailValidator, AlphaValidator, AlphaNumericValidator, AllowedCharacterValidator, PasswordValidation, FormErrorMessage } from 'src/app/utils/services/custom.validators';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  public profileForm: FormGroup = new FormGroup({
    userFirstName: new FormControl(null, [Validators.required, Validators.minLength(2), Validators.maxLength(100), AlphaValidator]),
    userLastName: new FormControl(null, [Validators.required, Validators.minLength(2), Validators.maxLength(100), AlphaValidator]),
    userName: new FormControl(null, [Validators.required, Validators.minLength(2), Validators.maxLength(100), AlphaNumericValidator]),
    userEmail: new FormControl(null, [Validators.required, Validators.email, Validators.minLength(2), Validators.maxLength(100), EmailValidator]),
    userContactNumber: new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(50), AllowedCharacterValidator])
  });

  public changePasswordForm: FormGroup = new FormGroup({
    userOldPassword: new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(32), PasswordValidator]),
    userNewPassword: new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(32), PasswordValidator]),
    userConfirmPassword: new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(32), PasswordValidator])
  });

  public userProfile = [];
  public isProcessing: boolean;
  public isSubmitted: boolean;
  public isProfileLoading: boolean;
  public passwordError: string;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _authSevice: AuthService,
    private _notificationSevice: NotificationService,
    private _encryptionSevice: EncryptionService,
    private _AdminService: AdminService
  ) { }

  ngOnInit(): void {
    this.getProfile();
  }

  /** Form Error Messages */
  getErrorMessage(control: string) {
    if (this.isSubmitted)
      return FormErrorMessage(this.profileForm, control);
  }

  getProfile() {
    this.isProfileLoading = true;
    this._AdminService.getProfile()
      .subscribe((apiResponse) => {
        this.userProfile = [apiResponse];
        this.profileForm.setValue({
          userFirstName: apiResponse.firstName,
          userLastName: apiResponse.lastName,
          userName: apiResponse.username,
          userEmail: apiResponse.email,
          userContactNumber: apiResponse.contactNumber,
        })
        this.isProfileLoading = false;
      });
  }

  updateProfile() {
    this.isSubmitted = true;
    if (this.profileForm.valid) {
      try {
        this.isProcessing = true;
        let formData = {
          email: this.profileForm.get("userEmail").value,
          firstName: this.profileForm.get("userFirstName").value,
          lastName: this.profileForm.get("userLastName").value,
          username: this.profileForm.get("userName").value,
          contactNumber: this.profileForm.get("userContactNumber").value
        }
        this._AdminService.updateProfile(formData)
          .pipe(
            finalize(() => {
              this.isProcessing = false;
            })
          ).subscribe((apiResponse) => {
            this.isProcessing = false;
            this.getProfile();
          });
      } catch (err) {
        this.isProcessing = false;
      }
    }
  }

  changePassword() {
    if (this.changePasswordForm.valid) {
      this.passwordError = "";
      if (this.changePasswordForm.get("userNewPassword").value != this.changePasswordForm.get("userConfirmPassword").value) {
        this.passwordError = "Password and Confirm password doesn't match";
        return;
      } else if (!PasswordValidation(this.changePasswordForm.get("userNewPassword").value)
      ) {
        this.passwordError =
          "Password length should be 6-32characters, contain 1 Uppercase 1 Lowercase, 1 Number, 1 Special Character";
        return;
      } else {
        try {
          this.isProcessing = true;
          let formData = {
            oldPassword: this.changePasswordForm.get("userOldPassword").value,
            newPassword: this.changePasswordForm.get("userNewPassword").value
          }
          this._AdminService.changePassword(formData)
            .pipe(
              finalize(() => {
                this.isProcessing = false;
              })
            ).subscribe((apiResponse) => {
              this.isProcessing = false;
              this.changePasswordForm.reset();
            });
        } catch (err) {
          this.isProcessing = false;
        }
      }
    }
  }

}
