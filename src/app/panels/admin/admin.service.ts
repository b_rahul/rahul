import { Injectable } from "@angular/core";
import { AuthService } from "src/app/utils/services/auth.service";
import { environment } from "src/environments/environment";
import { Observable } from "rxjs";
import { map } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: "root",
})
export class AdminService {
  constructor(private _authService: AuthService) { }

  /** PROFILE START */
  getProfile(data?: any): Observable<any> {
    return this._authService.get(
      `${environment.baseUrl}/v1/Member`,
      this._authService.getHeader(['application/json'])
    ).pipe(map(res => res));
  }

  updateProfile(data: any): Observable<any> {
    return this._authService.put(
      `${environment.baseUrl}/v1/Member`,
      data
    ).pipe(map(res => res));
  }

  changePassword(data: any): Observable<any> {
    return this._authService.put(
      `${environment.baseUrl}/v1/Member/ChangePassword`,
      data
    ).pipe(map(res => res));
  }
  /** PROFILE END */

  /** BANNER START */
  getBanners(data?: any): Observable<any> {
    return this._authService.get(
      `${environment.baseUrl}/v1/Member/Banner`,
      this._authService.getHeader(['application/json'])
    ).pipe(map(res => res));
  }

  addBanner(data: any): Observable<any> {
    return this._authService.post(
      `${environment.baseUrl}/v1/Member/Banner`,
      data
    ).pipe(map(res => res));
  }

  deleteBanner(data: any): Observable<any> {
    return this._authService.delete(
      `${environment.baseUrl}/v1/Member/Banner/` + data
    ).pipe(map(res => res));
  }

  /** BANNER END */

  /** CATEGORY START */
  getCategories(data?: any): Observable<any> {
    return this._authService.get(
      `${environment.baseUrl}/v1/Category`,
      this._authService.getHeader(['application/json'])
    ).pipe(map(res => res));
  }

  addCategory(data: any): Observable<any> {
    return this._authService.post(
      `${environment.baseUrl}/v1/Category`,
      data
    ).pipe(map(res => res));
  }

  updateCategory(data: any, categoryID: string): Observable<any> {
    return this._authService.put(
      `${environment.baseUrl}/v1/Category/` + categoryID,
      data
    ).pipe(map(res => res));
  }

  deleteCategory(data: any): Observable<any> {
    return this._authService.delete(
      `${environment.baseUrl}/v1/Category/` + data
    ).pipe(map(res => res));
  }

  addCategoryImage(data: any, categoryID: string): Observable<any> {
    return this._authService.post(
      `${environment.baseUrl}/v1/Category/Image/` + categoryID,
      data
    ).pipe(map(res => res));
  }

  deleteCategoryImage(data: any): Observable<any> {
    return this._authService.delete(
      `${environment.baseUrl}/v1/Category/Image/getById/` + data
    ).pipe(map(res => res));
  }
  /** CATEGORY END */

  /** PRODUCTS START */
  getProducts(data?: any): Observable<any> {
    return this._authService.get(
      `${environment.baseUrl}/v1/Product`,
      this._authService.getHeader(['application/json'])
    ).pipe(map(res => res));
  }

  addProduct(data: any): Observable<any> {
    return this._authService.post(
      `${environment.baseUrl}/v1/Product`,
      data
    ).pipe(map(res => res));
  }

  updateProduct(data: any, ProductID: string): Observable<any> {
    return this._authService.put(
      `${environment.baseUrl}/v1/Product/` + ProductID,
      data
    ).pipe(map(res => res));
  }

  deleteProduct(data: any): Observable<any> {
    return this._authService.delete(
      `${environment.baseUrl}/v1/Product/` + data
    ).pipe(map(res => res));
  }

  addProductImage(data: any, ProductID: string): Observable<any> {
    return this._authService.post(
      `${environment.baseUrl}/v1/Product/Image/` + ProductID,
      data
    ).pipe(map(res => res));
  }

  deleteProductImage(data: any): Observable<any> {
    return this._authService.delete(
      `${environment.baseUrl}/v1/Product/Image/getById/` + data
    ).pipe(map(res => res));
  }
  /** PRODUCTS END */

}
