import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './panels/shared/main/main.component';
import { NotFoundComponent } from './panels/shared/not-found/not-found.component';


const routes: Routes = [
  {
    path: 'App/Admin',
    loadChildren: () => import('./panels/admin/admin.module').then(m => m.AdminModule)
  },
  {
    path: '',
    loadChildren: () => import('../app/panels/front/front.module').then(m => m.FrontModule)
  },
  { 
    path: "Page-Not-Found", 
    component: NotFoundComponent
  },
  { 
    path: "**", 
    redirectTo: "Page-Not-Found" 
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
